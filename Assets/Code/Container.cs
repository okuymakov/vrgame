using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Container : MonoBehaviour
{
    public string Name;
    public GarbageType type;
    public int maxCapacity;
    public int curCapacity;

    private Color normalColor = Color.white;
    private Color errorColor = Color.red;
    private Color successColor = Color.green;

    public bool CollectWaste(Garbage garbage) {
        if (curCapacity != maxCapacity && garbage.type == type) {
         
            curCapacity++;
            return true;
            
        }
        return false;
    }

    public TextWithColor GetDisplayText(Garbage? garbage)  {
        if (curCapacity != maxCapacity)
        {
            if (garbage == null || garbage?.type == type)
            {
                return new TextWithColor($"{Name}\n{curCapacity} / {maxCapacity}", normalColor);

            } else
            {
                return new TextWithColor($"{Name}\n{curCapacity} / {maxCapacity}\nВыберите другой тип мусора", errorColor);
            }
            
        } 
        return new TextWithColor($"{Name}\n{curCapacity} / {maxCapacity}", successColor);
    }
}

public class TextWithColor
{
    public string text;
    public Color color;

    public TextWithColor(string text, Color color)
    {
        this.text = text;
        this.color = color;
    }
}

