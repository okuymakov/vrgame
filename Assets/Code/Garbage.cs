using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Garbage : MonoBehaviour
{
    public GarbageType type;
}

public enum GarbageType
{
    PLASTIC,
    GLASS,
    METAL,
    PAPER,
    OTHER,
    BATTERY
}