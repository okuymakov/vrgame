using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class Player : MonoBehaviour
{
    public LayerMask WasteLayer;
    public GameObject LeftHandWaste = null;
    public GameObject RightHandWaste = null;
    public XRDirectInteractor rHand;
    public XRDirectInteractor lHand;
    

    void Update()
    {
        var lSelectTarget = lHand.selectTarget?.gameObject;
        if (lSelectTarget?.layer == WasteLayer)
        {
            LeftHandWaste = lSelectTarget;
        }

        var rSelectTarget = rHand.selectTarget?.gameObject;
        if (rSelectTarget?.layer == WasteLayer)
        {
            RightHandWaste = rSelectTarget;
        }
    }

    public void ClearLeftHand()
    {
        Destroy(LeftHandWaste);
        LeftHandWaste = null;
    }

    public void ClearRightHand()
    {
        Destroy(RightHandWaste);
        RightHandWaste = null;
    }
}
