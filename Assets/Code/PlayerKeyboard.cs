using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerKeyboard : MonoBehaviour
{
    [SerializeField] private LayerMask PickupLayer;
    [SerializeField] private Camera PlayerCamera;
    [SerializeField] private float ThrowingForce;
    [SerializeField] private float PickupRange;
    [SerializeField] private Transform Hand;
    public GameObject LeftHandWaste = null;


    private Rigidbody CurrentObjectRigidbody;
    private Collider CurrentObjectCollider;

    void Update()
    {
        Ray Pickupray = new Ray(PlayerCamera.transform.position, PlayerCamera.transform.forward);
        var isAccessible = Physics.Raycast(Pickupray, out RaycastHit hitInfo, PickupRange, PickupLayer);


        if (isAccessible)
        {
            Debug.Log("Press E");
        }
        
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (isAccessible)
            {
                if (LeftHandWaste)
                {
                    CurrentObjectRigidbody.isKinematic = false;
                    CurrentObjectCollider.enabled = true;

                    CurrentObjectRigidbody = hitInfo.rigidbody;
                    CurrentObjectCollider = hitInfo.collider;

                    CurrentObjectRigidbody.isKinematic = true;
                    CurrentObjectCollider.enabled = false;
                }
                else
                {
                    CurrentObjectRigidbody = hitInfo.rigidbody;
                    CurrentObjectCollider = hitInfo.collider;

                    CurrentObjectRigidbody.isKinematic = true;
                    CurrentObjectCollider.enabled = false;

                    LeftHandWaste = hitInfo.transform.gameObject;

                }

                return;
            }

            if (LeftHandWaste)
            {
                CurrentObjectRigidbody.isKinematic = false;
                CurrentObjectCollider.enabled = true;

                CurrentObjectRigidbody = null;
                CurrentObjectCollider = null;
                LeftHandWaste = null;
            }
        }

        if (LeftHandWaste)
        {
            CurrentObjectRigidbody.position = Hand.position;
            CurrentObjectRigidbody.rotation = Hand.rotation;
        }


    }

    public void ClearLeftHand()
    {
        Destroy(LeftHandWaste);
        LeftHandWaste = null;
    }

}
