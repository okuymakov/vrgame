using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeWaste: MonoBehaviour
{
    [SerializeField] private LayerMask PickupLayer;
    [SerializeField] private Camera PlayerCamera;
    [SerializeField] private float ThrowingForce;
    [SerializeField] private float PickupRange;
    [SerializeField] private Transform Hand;
    [SerializeField] private GameObject PickUpText;
    [SerializeField] private Player Player;

    private Rigidbody CurrentObjectRigidbody;
    private Collider CurrentObjectCollider;

    void Update()
    {
        Ray Pickupray = new Ray(PlayerCamera.transform.position, PlayerCamera.transform.forward);
        var isAccessible = Physics.Raycast(Pickupray, out RaycastHit hitInfo, PickupRange, PickupLayer);

        PickUpText.SetActive(isAccessible);

        if (Input.GetKeyDown(KeyCode.E))
        {
            if (isAccessible)
            {
                if (Player.LeftHandWaste)
                {
                    CurrentObjectRigidbody.isKinematic = false;
                    CurrentObjectCollider.enabled = true;

                    CurrentObjectRigidbody = hitInfo.rigidbody;
                    CurrentObjectCollider = hitInfo.collider;

                    CurrentObjectRigidbody.isKinematic = true;
                    CurrentObjectCollider.enabled = false;
                }
                else
                {
                    CurrentObjectRigidbody = hitInfo.rigidbody;
                    CurrentObjectCollider = hitInfo.collider;

                    CurrentObjectRigidbody.isKinematic = true;
                    CurrentObjectCollider.enabled = false;

                    Player.LeftHandWaste = hitInfo.transform.gameObject;

                }

                return;
            }

            if (Player.LeftHandWaste)
            {
                CurrentObjectRigidbody.isKinematic = false;
                CurrentObjectCollider.enabled = true;

                CurrentObjectRigidbody = null;
                CurrentObjectCollider = null;
                Player.LeftHandWaste = null;
            }
        }

        if (Player.LeftHandWaste)
        {
            CurrentObjectRigidbody.position = Hand.position;
            CurrentObjectRigidbody.rotation = Hand.rotation;
        }


    }
}