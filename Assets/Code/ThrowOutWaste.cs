using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ThrowOutWaste : MonoBehaviour
{
    [SerializeField] private LayerMask ContainerLayer;
    [SerializeField] private float CollectRange;
    [SerializeField] private Camera PlayerCamera;
    [SerializeField] private Player Player;
    [SerializeField] private GameObject HelperText;

    void Update()
    {
        Ray ContainerRay = new Ray(PlayerCamera.transform.position, PlayerCamera.transform.forward);
        var isAccessible = Physics.Raycast(ContainerRay, out RaycastHit hitInfo, CollectRange, ContainerLayer);

        HelperText.SetActive(isAccessible);

        if (isAccessible)
        {
            var container = hitInfo.transform.gameObject.GetComponent<Container>();
            var text = HelperText.GetComponent<TextMeshProUGUI>();
            var textWithColor = container.GetDisplayText(Player?.RightHandWaste?.GetComponent<Garbage>());
            text.text = textWithColor.text;
            text.color = textWithColor.color;
   
        }

        if (false)
        {
            if (isAccessible)
            {
                var container = hitInfo.transform.gameObject.GetComponent<Container>();

                if (Player.LeftHandWaste) {
                    var garbage = Player.LeftHandWaste.GetComponent<Garbage>();
                    if (container.CollectWaste(garbage)) {
                        Player.ClearLeftHand();
                        return;
                    }
                }

                if (Player.RightHandWaste)
                {
                    var garbage = Player.RightHandWaste.GetComponent<Garbage>();
                    if (container.CollectWaste(garbage))
                    {
                        Player.ClearRightHand();
                        return;
                    }
                }
            }
        }
    }
}