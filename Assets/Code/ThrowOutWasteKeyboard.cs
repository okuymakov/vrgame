using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ThrowOutWasteKeyboard : MonoBehaviour
{
    [SerializeField] private LayerMask ContainerLayer;
    [SerializeField] private float CollectRange;
    [SerializeField] private Camera PlayerCamera;
    [SerializeField] private PlayerKeyboard Player;
    [SerializeField] private GameObject HelperText;

    void Update()
    {
        Ray ContainerRay = new Ray(PlayerCamera.transform.position, PlayerCamera.transform.forward);
        var isAccessible = Physics.Raycast(ContainerRay, out RaycastHit hitInfo, CollectRange, ContainerLayer);

        HelperText.SetActive(isAccessible);

        if (isAccessible)
        {
            var container = hitInfo.transform.gameObject.GetComponent<Container>();
            var text = HelperText.GetComponent<TextMeshProUGUI>();
            var textWithColor = Player.LeftHandWaste == null? container.GetDisplayText(null) : container.GetDisplayText(Player.LeftHandWaste.GetComponent<Garbage>());
            text.text = textWithColor.text;
            text.color = textWithColor.color;

            Debug.Log(textWithColor.text);
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (isAccessible)
            {
                var container = hitInfo.transform.gameObject.GetComponent<Container>();

                if (Player.LeftHandWaste)
                {
                    var garbage = Player.LeftHandWaste.GetComponent<Garbage>();
                    if (container.CollectWaste(garbage))
                    {
                        Player.ClearLeftHand();
                        return;
                    }
                }

           
            }
        }
    }
}
